module seg_led_static_top (
    input sys_clk ,
    input sys_rst_n,
    
    //seg_led interface
    output [5:0] seg_sel , 
    output [7:0] seg_led 
);

parameter TIME_SHOW = 25'd25000_000;
wire add_flag; 

seg_led_static u_seg_led_static (
    //modele clock
    .clk (sys_clk ),
    .rst_n (sys_rst_n), 
    //seg_led interface
    .seg_sel (seg_sel ), 
    .seg_led (seg_led ), 
    .add_flag (add_flag ) 
);
    
time_count #(.MAX_NUM(TIME_SHOW)) u_time_count(
    //system clock
    .clk (sys_clk ), 
    .rst_n (sys_rst_n),
    //user interface
    .flag (add_flag ) 
);
endmodule
